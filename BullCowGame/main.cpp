#pragma once
/*
This is the console executable that makes use of the BullCow class.
This acts as the view in a MVC pattern, and is responsible for all user interaction.

For game logic see the FBullCowGame class.
*/
#include <iostream>
#include <string>
#include "FBullCowGame.h"

using FText = std::string;
using int32 = int;

void PrintIntro();
void PlayGame();
FText GetValidGuess();
bool AskToPlayAgain();
void PrintGameSummary();


FBullCowGame BCGame; //create an instance of new game

int main() 
{
	bool bPlayAgain = false;
	do 
	{
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} 
	while (bPlayAgain);

	return 0;
}

void PrintIntro()
{
	// Introduce a game
	std::cout << "\n\nWelcome to Bulls and Cows by" << std::endl;
	std::cout << " __        __  ___  ___  __   __            \n";
	std::cout << "|__)  /\\  |__)  |  |__  |__) |__) |    \\ / \n";
	std::cout << "|__) /~~\\ |  \\  |  |___ |  \\ |__) |___  |  \n";
	std::cout << "\nCan you guess " << BCGame.GetHiddenWordLenght() << "-letter isogram?\n";
	return;
}

void PlayGame()
{
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();
	
	
	
	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries)
	{
		FText Guess = GetValidGuess();
		
		//Submit valid guess to the game
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		//Print number of bulls and cows
		std::cout << "Bulls = " << BullCowCount.Bulls << ". Cows = " << BullCowCount.Cows << "\n\n";
	}
	PrintGameSummary();
	return;
}

FText GetValidGuess()
{
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::INVALID_STATUS;
	do
	{
		int32 CurrentTry = BCGame.GetCurrentTry();
		std::cout << "Try " << CurrentTry << " of " << BCGame.GetMaxTries() << ". Enter your guess: ";
		std::getline(std::cin, Guess);
		Status = BCGame.CheckGuessValidity(Guess);
		switch (Status)
		{
		case EGuessStatus::Wrong_Lenght:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLenght() << "-letter word.\n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Please enter an isogram (word without repeating letters).\n\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please enter your guess in lowercase letters.\n\n";
			break;
		default:
			// assume the guess is valid
			break;
		}
	} while (Status != EGuessStatus::OK);
	return Guess;
}

bool AskToPlayAgain()
{
	std::cout << "Do you want to play again with the same hidden word? (y/n)";
	FText Reponse = "";
	std::getline(std::cin, Reponse);

	if ((Reponse[0] == 'Y') ||  (Reponse[0] == 'y') || (Reponse[0] == 'T') || (Reponse[0] == 't'))
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon())
	{
		std::cout << "WELL DONE, YOU WIN!!!\n\n";
	}
	else
	{
		std::cout << "GAME OVER. Better luck next time!\n\n";
	}
}