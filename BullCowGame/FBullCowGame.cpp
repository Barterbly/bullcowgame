#pragma once
#include "FBullCowGame.h"
#include <map>
#define TMap std::map

FBullCowGame::FBullCowGame() { Reset(); }


int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
int32 FBullCowGame::GetHiddenWordLenght() const { return MyHiddenWord.length(); }
bool FBullCowGame::IsGameWon() const { return bGameIsWon; }

int32 FBullCowGame::GetMaxTries() const 
{
	TMap<int32, int32> WordLenghtToMaxTries{ {3, 4}, {4,7}, {5,10}, {6,10}, {7,20} };
	return WordLenghtToMaxTries[MyHiddenWord.length()];
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const 
{ 
	if (!IsIsogram(Guess)) //if the guess is not an isogram
	{
		return EGuessStatus::Not_Isogram; // return "Not_Isogram" status
	}
	else if (!IsLowercase(Guess)) //if the guess isn't all lowercase
	{
		return EGuessStatus::Not_Lowercase; // return "Not_Lowercase" status
	}
	else if (Guess.length() != GetHiddenWordLenght()) //if the guess lenght is wrong
	{
		return EGuessStatus::Wrong_Lenght; // return "Wrong_Lenght" status
	}
	else //otherwise
	{
		return EGuessStatus::OK; // return "OK" status
	}
}		

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "planet";
	
	MyHiddenWord = HIDDEN_WORD;
	MyCurrentTry = 1;
	bGameIsWon = false;
	return;
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	if (Word.length() <= 1) { return true; }
	TMap<char, bool> LetterSeen; // setup our map
	for (auto Letter : Word) //for all letters of word
	{
		Letter = tolower(Letter); // handle mixed letter case
		if (LetterSeen[Letter]) // if the letter is in the map
		{
			return false; // we do NOT have an isogram
		}
		else
		{
			LetterSeen[Letter] = true;// add letter to the map
		}
	}
	return true;
}

bool FBullCowGame::IsLowercase(FString Word) const
{
	for (auto Letter : Word)
	{
		if (!islower(Letter))// if not a lowercase
		{
			return false;// return false
		}	
	}
	return true;
}

// receives a VALID guess, increments turn and return count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++;
	FBullCowCount BullCowCount;
	int32 WordLenght = MyHiddenWord.length(); // should be true, because validation is made in different method

	// loop through all letters in the hidden word
	for (int32 MHWCharacter = 0; MHWCharacter < WordLenght; MHWCharacter++)
	{
		//compare letters against guess
		for (int32 GCharacter = 0; GCharacter < WordLenght; GCharacter++)
		{
			// if they match then
			if (Guess[GCharacter] == MyHiddenWord[MHWCharacter])
			{
				//if they are in the same place
				if (MHWCharacter == GCharacter)
				{
					BullCowCount.Bulls++; //increment Bulls
				}
				else
				{
					BullCowCount.Cows++; //increment cows if not
				}
			}
		}
	}
	if (BullCowCount.Bulls == WordLenght)
	{
		bGameIsWon = true;
	}
	else
	{
		bGameIsWon = false;
	}
	return BullCowCount;
}
